package maratona;

import java.util.ArrayList;
import java.util.List;

public class Equipe {
	public int id;
	public List<Aluno> alunos = new ArrayList<>();
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append(id);
		builder.append("\n");
		
		for(Aluno aluno : alunos) {
			builder.append(aluno + "\n");
		}
		
		return builder.toString();
	}
}
