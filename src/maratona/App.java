package maratona;

import java.util.List;

public class App {
	
	public static void main(String[] args) {
		Arquivo arquivo = new Arquivo("src/alunos.csv");
		List<Aluno> alunos = arquivo.ler();
		List<Equipe> equipes = new GeradorEquipes(alunos, 3).gerar();
		
		Impressora.imprimir(equipes);
	}
}
