package maratona;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GeradorEquipes {
	private List<Aluno> alunos;
	private List<Equipe> equipes = new ArrayList<>();
	private int idAtual = 1;
	private int quantidadeAlunos;
	
	public GeradorEquipes(List<Aluno> alunos, int quantidadeAlunos) {
		this.alunos = alunos;
		this.quantidadeAlunos = quantidadeAlunos;
		
		Collections.shuffle(alunos);
	}
	
	public List<Equipe> gerar(){
		while(alunos.size() > 0) {
			equipes.add(gerarEquipe());
		}
		
		return equipes;
	}
	
	private Equipe gerarEquipe() {
		Equipe equipe = new Equipe();
		equipe.id = idAtual;
		idAtual++;
		
		while(equipe.alunos.size() < quantidadeAlunos && alunos.size() > 0) {
			equipe.alunos.add(alunos.remove(0));
		}
		
		return equipe;
	}
}
